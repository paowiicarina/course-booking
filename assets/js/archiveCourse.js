let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");
let isActive = document.querySelector("#isActiveTrue");

	isActive.addEventListener("click", () => {

		fetch(`http://localhost:3000/api/courses/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				alert("Successfully Archived!")
				window.location.replace("./courses.html")
			}else{
				alert("Something went wrong!")
			}
		});
	});
