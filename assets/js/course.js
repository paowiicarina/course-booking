
//window.location.search returns the query string part of the URL
//console.log(window.location.search);

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local device
let userId = localStorage.getItem("id");

let adminUser = localStorage.getItem("isAdmin");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let enrollees = document.querySelector("#enrollees");

fetch(`http://localhost:3000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if(adminUser == "false" || !adminUser){

		enrollContainer.innerHTML = `<button id="enrollButton" class="course-button btn-lg btn-warning btn-block">Enroll</button>`

		let enrollButton = document.querySelector("#enrollButton")

		enrollButton.addEventListener("click", () => {
			
			fetch("http://localhost:3000/api/users/enroll", {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
					userId: userId
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					//enrollment is successful
					alert("Thank you for enrolling with us! Fly high!")
					window.location.replace("./courses.html")
				}else{
					alert("Enrollment failed")
				}
			});
		});
		
	}else{

		enrolleesContainer.innerHTML = `<button id="enrollButton" class="course-button btn-lg btn-warning btn-block">Enrollees</button>`
		
		let enrolleesData;
		
		let fullName = []
		let fullName2 = []

		if(data.length < 1){
		enrolleesData = "No courses available.";
		}else{
		
			enrollees = data.enrollees
			console.log(data)

			enrolleesData = enrollees.map(user => {
				userId = user.userId

				fetch(`http://localhost:3000/api/users/${userId}`,{
				})
				.then(res => res.json())
				.then(data => {

					usersName1 = data.firstName

					usersName2 = data.lastName
					
					fullName = `${usersName1} ${usersName2} `
				
					fullName2 = fullName2.concat(fullName)					



					let container = document.querySelector("#enrollees");
					container.innerHTML = `<b>Users Enrolled:</b><br>${fullName2}`
				})	
			})
		};
	}
	
});
