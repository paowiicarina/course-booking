let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId')

let token = localStorage.getItem('token')

let userId = localStorage.getItem("id");

let deleteContainer = document.querySelector("#deleteContainer");

 fetch(`http://localhost:3000/api/courses/${courseId}`,{
 	method: 'DELETE',
 	headers: {
 		'Content-Type': 'application/json',
 		'Authorization': `Bearer ${token}`
 	},
 })
 .then(res => {
 	return res.json()
.then(data => {
		if(data === true){	
		alert("Deleted successfully")		
			window.location.replace('./courses.html')
		}else{
			alert('Course is still on going')
		}
	})
})
