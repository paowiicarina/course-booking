let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");


let userId = params.get('user')
let courseId = params.get('courseId')
let userName = document.querySelector("#userName");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");
let courses = document.querySelector("#courses");
let coursesEnrolled = document.querySelector("#coursesEnrolled");
let profileContainer = document.querySelector("#profileContainer");



fetch('http://localhost:3000/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	userName.innerHTML = data.firstName + " " + data.lastName
	email.innerHTML = data.email
	mobileNo.innerHTML = data.mobileNo

	let courseIds = []
	let courseNames = []

	for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

	fetch('http://localhost:3000/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
	}

	let coursesEnrolled = data.enrollments.map (course => {
		courseIds.push(course.courseID)
	})

	let coursesList = courseNames.join("\n")
  //Course names are here in coursesList
 })
})