let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-dark">Add Course</a>
	</div>`
}

//fetch the courses from our API
fetch('http://localhost:3000/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Edit</a>
							<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Delete</a>
							<a href="./ArchiveCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Delete</a>
							<a href="./enrollees.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Reactivate</a>`

			}			

			return (
					`<div class="col-md-6 my-3">
	<div class="card text-white bg-dark mb-3">

		<div class="card-body">
			<img class="card-img-top" src="${'https://adventuretimenipaowii.files.wordpress.com/2020/09/20200916_140151-2.gif?w=350'}" alt="card img">
			<h5 class="card-title">${course.name}</h5>
			<p class="card-text text-left">${course.description}</p>
			<p class="card-text text-right">₱ ${course.price}</p>
		</div>

		<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})
